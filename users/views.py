from django.shortcuts import render
from django.urls import reverse_lazy
from django.views import generic
from django.contrib.auth.models import User
import pyrebase

#ติดต่อfirebase
config = {
'apiKey': "AIzaSyBwg4BeVjNdZob1mnXUtXSbljQzN47vjf0",
'authDomain': "cpanel-b511a.firebaseapp.com",
'databaseURL': "https://cpanel-b511a.firebaseio.com",
'projectId': "cpanel-b511a",
'storageBucket': "cpanel-b511a.appspot.com",
'messagingSenderId': "554934818907"
}
firebase = pyrebase.initialize_app(config)
auth = firebase.auth()
user = auth.sign_in_with_email_and_password("akkaradet.pu.60@ubu.ac.th","242613800")
db = firebase.database()
storage = firebase.storage()


from .forms import CustomUserCreationForm

class SignUp(generic.CreateView):
    form_class = CustomUserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'signup.html'

def index(req):
#อ่านข้อมูลจากirbase
    getdata = db.child("test").child("data").shallow().get().val()
    listitem = []
    for i in getdata:
        listitem.append(i)
    listitem.sort(reverse=True)
    dataitemread = []
    for i in listitem:
        data1=db.child("test").child("data").child(i).child("title").get().val()
        dataitemread.append(data1)
    datadescripread = []
    for i in listitem:
        data2=db.child("test").child("data").child(i).child("descrip").get().val()
        datadescripread.append(data2)
    comb_lis = zip(dataitemread,datadescripread,listitem)
#โยนข้อมูลให้กับหน้าindexเพื่อนำไปโชว์
    print(dataitemread)
    print(datadescripread)
    print(listitem)
    return render(req,'page/index.html',{'comb_lis':comb_lis})

def adddata(req):
    return render(req,'page/adddata.html')
    
def postdata(req):
#ดึงข้อมูลจาก adddata.html
    data=req.GET
    title=data.getlist('title')
    descrip=data.getlist('descrip')
#push ข้อมูลลงfirbse
    data = {
        "title":str(title[0]),
        "descrip":str(descrip[0])
    }
    db.child("test").child("data").push(data)
#อ่านข้อมูลจากirbase
    getdata = db.child("test").child("data").shallow().get().val()

    listitem = []
    for i in getdata:
        listitem.append(i)
    listitem.sort(reverse=True)

    dataitemread = []
    for i in listitem:
        data1=db.child("test").child("data").child(i).child("title").get().val()
        dataitemread.append(data1)
    datadescripread = []

    for i in listitem:
        data2=db.child("test").child("data").child(i).child("descrip").get().val()
        datadescripread.append(data2)
    comb_lis = zip(dataitemread,datadescripread,listitem)
#โยนข้อมูลให้กับหน้าindexเพื่อนำไปโชว์
    print(dataitemread)
    print(datadescripread)
    print(listitem)
    return render(req,'page/index.html',{'comb_lis':comb_lis})